#!/bin/sh

output=~/Dropbox/journal.html
dir_with_md="$1/journal"

find "$1" -type f -name "*.md" | sort | python3 /home/ch/code/gitlab/chelmertz/markdown-overview/main.py > $output && xdg-open $output
