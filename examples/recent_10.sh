#!/bin/sh

output=~/Dropbox/index.html
dir_with_md=$1

find "$1" -type f -name "*.md" -printf "%T@ %p\n" | sort -n | tail -n10 | cut -d ' ' -f2 | python3 /home/ch/code/gitlab/chelmertz/markdown-overview/main.py > $output && xdg-open $output
