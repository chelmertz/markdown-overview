# code: utf-8

from difflib import SequenceMatcher
from mdx_linkify.mdx_linkify import LinkifyExtension
import argparse
import collections
import logging
import markdown
import os
import re
import sys

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.addHandler(logging.StreamHandler(sys.stderr))

def common_prefix(strings):
    """
    >>> common_prefix(("/a/dir/hello", "/a/dir/bye"))
    '/a/dir/'
    >>> common_prefix(("/a/dir/hello123", "/a/dir/hello456"))
    '/a/dir/'
    """
    longest = ""

    for f in strings:
        if longest == "":
            longest = f
            continue

        new_longest = SequenceMatcher(None, longest, f).find_longest_match(0, len(longest), 0, len(f))

        longest = longest[new_longest.a:new_longest.a + new_longest.size]

    only_directories = re.match(r'(.*)/', longest)
    if only_directories:
        return only_directories.group()

    return longest

def toc_html_by_initial(toc):
    html = '<div class="toc">'
    by_initial = collections.defaultdict(list)

    for toc_entry in toc:
        if toc_entry['level'] != 1:
            continue

        by_initial[toc_entry['name'][0]].append(toc_entry)

    for initial, entries in by_initial.items():
        html += "\n<div class='by_initial'>"
        html += "\n<h3>" + initial.upper() + "</h3>"
        html += "\n<ul>"

        for entry in entries:
            html += "\n<li><a href='#" + entry['id'] + "'>" + entry['name'] + "</a></li>"

        html += "\n</ul>\n</div>"

    return html + "</div>"

def main(files, css):
    md = ''
    file_count = 0
    titles = []

    prefix = common_prefix(files)
    log.info("Common prefix: " + str(prefix))

    for filename in files:
        with open(filename, 'r') as f:
            file_count += 1
            title = filename.replace(prefix, '').replace('.md', '')
            titles.append(title)

            md += "\n<article markdown=1>\n"
            md += '# ' + title + "\n\n"
            for line in f.readlines():
                md += re.sub(r'^(#+)', r'\1#', line)
            md += "\n</article>\n"

    log.info("Markdown files: " + str(file_count))

    formatter = markdown.Markdown(
            output_format='html5',
            extensions=[
                'toc',
                'tables',
                LinkifyExtension(),
                'fenced_code',
                'extra'
                ],
            extension_configs={
                'toc': {
                    'marker': '',
                    'toc_depth': 1
                    }
                }
            )

    contents = formatter.convert(md)

    toc = toc_html_by_initial(formatter.toc_tokens)

    contents = """<!doctype html>
    <html>
    <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    {0}
    </style>
    </head>
    <body>
    {1}
    {2}
    </body></html>""".format(css, toc, contents)

    return contents


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    this_dir = os.path.dirname(os.path.realpath(__file__))

    parser.add_argument('--cssfile', type=argparse.FileType('r', encoding='UTF-8'), default='{0}/markdown_overview_default.css'.format(this_dir))
    parser.add_argument('--mdfile', help='Pass filenames on STDIN, like \'find . -name "*.md"\'',
            type=argparse.FileType('r', encoding='UTF-8'), default=sys.stdin, nargs='?')

    args = parser.parse_args()

    log.info('CSS filename: {0}'.format(args.cssfile.name))

    print(main(list(map(lambda x: x.strip(), args.mdfile)), args.cssfile.read()))
