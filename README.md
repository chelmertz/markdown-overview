Turns markdown files into a single HTML file, somewhat easy to read, without any configuration.

# Screenshots of output

## Table of contents

<a href="screenshots/toc.png"><img src="screenshots/toc.png" alt="toc"/></a>

## A post
<a href="screenshots/post.png"><img src="screenshots/post.png" alt="post"/></a>

## A small workflow demo, at asciinema

[![asciicast](https://asciinema.org/a/224250.svg)](https://asciinema.org/a/224250)

# Features

- attempts to be zero config, it just needs full paths to markdown files on
  stdin
- output in the form of a single file
	- inlined CSS
	- easy to distribute (I put my output index.html in Dropbox, so I can
	  view it on my cell phone)
	- enables the browser's search function
- table of contents, wrapped horizontally with flexbox to minimize
  unnecessary scrolling
- mobile friendly output
- automatically generates `<a>` elements from URLs
- minimal CSS formatting
	- max-width of text containers
	- horizontal wrapping
	- readable on mobile devices
- no javascript

# Usage

	$ find some-path -type f -name "*.md" | python3 main.py > index.html

See the examples folder for some more tips.

## Protip: generate output with a simple command

Setup shortcut in ~/.bashrc

	function m() {
	  output=/tmp/index.html
	  find "/path/to/markdown/files" -type f -name "*.md" | python3 /path/to/markdown-overview/main.py > $output && xdg-open $output
	}

Reload .bashrc

	$ source ~/.bashrc

Triggers a fresh generation of your markdown files, and opens the result in
your web browser.

	$ m


# Installation of required python modules

	$ python3 -mpip install --user markdown mdx_linkify

# About

- Author: Carl Helmertz (helmertz@gmail.com)
- Git repo: [https://gitlab.com/chelmertz/markdown-overview](https://gitlab.com/chelmertz/markdown-overview)
- License: MIT
